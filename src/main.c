
#include "stm32H743custom.h"


//*pODR = 0x1;			//OUTPUT HIGH - PIN0;
//*pODR = 0x81;			//OUTPUT HIGH - PIN0, PIN7;
//*pODR = 0x4081;		//OUTPUT HIGH - PIN_0, PIN7, PIN14;
//*pODR = 0x0;			//ALL LOW;

//*pODR = 0x4080;		//OUTPUT HIGH - PIN7, PIN14;
//*pODR = 0x4000;		//OUTPUT HIGH - PIN14;

void sequencial1(GPIOx_t *pGPIO, uint8_t direction) {

	if(direction == 1){
		if (GPIOx_readPortState(pGPIO) == 0x0) {
			PINx_setPinState(pGPIO, PIN_0, HIGH);
			return;
		} else if (GPIOx_readPortState(pGPIO) == 0x1) {
			//PINx_setPinState(pGPIO, PIN_0, HIGH);
			PINx_setPinState(pGPIO, PIN_7, HIGH);
			return;

		} else if (GPIOx_readPortState(pGPIO) == 0x81) {
		//	PINx_setPinState(pGPIO, PIN_0, HIGH);
		//	PINx_setPinState(pGPIO, PIN_7, HIGH);
			PINx_setPinState(pGPIO, PIN_14, HIGH);
			return;

		} else if (GPIOx_readPortState(pGPIO) == 0x4081) {
			GPIOx_setPortState(pGPIO, 0x0 );
			return;
		}
	}else{
		if (GPIOx_readPortState(pGPIO) == 0x0) {
			PINx_setPinState(pGPIO, PIN_14, HIGH);
			return;
		} else if (GPIOx_readPortState(pGPIO) == 0x4000) {
		//	PINx_setPinState(pGPIO, PIN_14, HIGH);
			PINx_setPinState(pGPIO, PIN_7, HIGH);
			return;
		} else if (GPIOx_readPortState(pGPIO) == 0x4080) {
		//	PINx_setPinState(pGPIO, PIN_14, HIGH);
		//	PINx_setPinState(pGPIO, PIN_7, HIGH);
			PINx_setPinState(pGPIO, PIN_0, HIGH);
			return;
		} else if (GPIOx_readPortState(pGPIO) == 0x4081) {
			GPIOx_setPortState(pGPIO, 0x0);
			return;
		}
	}
	/*
	if(direction){
		PINx_setPinState(pGPIO, PIN_0, HIGH);
	}else{
		PINx_setPinState(pGPIO, PIN_0, LOW);
	}
	*/
}

void delay(uint32_t wait){
	for(int i=0; i<wait; i++){

	}
}
int main(void){

	RCC_t pRCC;// = (uint32_t*)RCC_BASEADDRES;
	RCCsetup(&pRCC, RCC_BASEADDRES);
//	RCC_AHB4_ENR_PortSetup(&pRCC, 0x00000002U);
	RCC_AHB4_ENR_BitSetup(&pRCC, GPIOB);
	RCC_AHB4_ENR_BitSetup(&pRCC, GPIOC);

	GPIOx_t gpioC;
	GPIOx_setup(&gpioC, GPIOC_BASEADDRES);
	PINx_setPinMODE(&gpioC, PIN_13, MODE_INPUT);
	PINx_setPinTYPE(&gpioC, PIN_13, TYPE_PUSH_PULL);

	GPIOx_t gpioB;
	GPIOx_setup(&gpioB, GPIOB_BASEADDRES);
	/*
	GPIOx_setPortMODE(&gpioB, 0x10004001);
	GPIOx_setPortTYPE(&gpioB, 0x0);
	GPIOx_setPortSPEED(&gpioB, 0x20008C02);
	GPIOx_setPortPUPD(&gpioB, 0x25);
	GPIOx_setPortODR(&gpioB, 0x4081);
	 */
	PINx_setPinMODE(&gpioB, PIN_0, MODE_OUTPUT);
	PINx_setPinTYPE(&gpioB, PIN_0, TYPE_PUSH_PULL);
	PINx_setPinSPEED(&gpioB, PIN_0, SPEED_HIGH);
	PINx_setPinPUPD(&gpioB, PIN_0, PUPD_DOWN);

	PINx_setPinMODE(&gpioB, PIN_7, MODE_OUTPUT);
	PINx_setPinTYPE(&gpioB, PIN_7, TYPE_PUSH_PULL);
	PINx_setPinSPEED(&gpioB, PIN_7, SPEED_HIGH);
	PINx_setPinPUPD(&gpioB, PIN_7, PUPD_DOWN);

	PINx_setPinMODE(&gpioB, PIN_14, MODE_OUTPUT);
	PINx_setPinTYPE(&gpioB, PIN_14, TYPE_PUSH_PULL);
	PINx_setPinSPEED(&gpioB, PIN_14, SPEED_HIGH);
	PINx_setPinPUPD(&gpioB, PIN_14, PUPD_DOWN);

//	PINx_setPinState(&gpioB, PIN_0, HIGH);
//	PINx_setPinState(&gpioB, PIN_7, HIGH);

	uint8_t direction = 1;
	while(1){
		if(PINx_readPinState(&gpioC, PIN_13) == HIGH){
			direction = ~direction;
			GPIOx_setPortState(&gpioB, 0x0);
			delay(500000);
		}
		sequencial1(&gpioB, direction);
		delay(5000000);
	}

}
