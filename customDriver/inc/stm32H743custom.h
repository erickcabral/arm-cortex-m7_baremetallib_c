/*
 * stm32H743custom.h
 *
 *  Created on: 6 Feb 2020
 *      Author: Erick Cabral
 */

#ifndef STM32H743CUSTOM_H_
#define STM32H743CUSTOM_H_

#include <stdint.h>
/* CUSTOIM STM32H743 DRIVER */

/*!< DEFAULT VARIABLES >*/
#define SET				1
#define RESET			0
#define ENABLE			SET
#define DISABLE			DISABLE
#define HIGH			SET
#define LOW				RESET

#define PIN_0			0
#define PIN_1			1
#define PIN_2			2
#define PIN_3			3
#define PIN_4			4
#define PIN_5			5
#define PIN_6			6
#define PIN_7			7
#define PIN_8			8
#define PIN_9			9
#define PIN_10			10
#define PIN_11			11
#define PIN_12			12
#define PIN_13			13
#define PIN_14			14
#define PIN_15			15

/*!< AHB4 BUS PERIPHERALS >*/
#define RCC_BASEADDRES					0x58024400U

/*!< BASE ADDRESSES DEFAULT >*/

#define APB1_BASEADDRESS				0x40000000U
#define APB2_BASEADDRESS				0x40010000U
#define APB3_BASEADDRESS				0x50000000U
#define APB4_BASEADDRESS				0x58000000U

#define AHB1_BASEADDRESS				0x40017400U
#define AHB2_BASEADDRESS				0x48022800U
#define AHB3_BASEADDRESS				0x51000000U
#define AHB4_BASEADDRESS				0x58020000U

/*
 *  GPIO SETUP STRUCTURE
 */

//#define  OTYPER		(MODER+0x04U)	//OFFSET -> 0x04
/*
 volatile uint32_t OTYPER;			//OFFSET -> 0x04
 volatile uint32_t OSPEEDR;			//OFFSET -> 0x08
 volatile uint32_t PUPDR;			//OFFSET -> 0x0C
 volatile uint32_t IDR;				//OFFSET -> 0x10
 volatile uint32_t ODR;				//OFFSET -> 0x14
 volatile uint32_t BSRR;				//OFFSET -> 0x18
 volatile uint32_t LCKR;				//OFFSET -> 0x1C
 volatile uint32_t AFRL;				//OFFSET -> 0x20
 volatile uint32_t AFRH;				//OFFSET -> 0x24
 */

/*

 class RCC {
 private:
 volatile uint32_t CR;				//OFFSET -> 0x000

 volatile uint32_t* pICSR;				//OFFSET -> 0x004
 volatile uint32_t* pHSICFGR;			//OFFSET -> 0x004
 volatile uint32_t* pCRRCR;			//OFFSET -> 0x008

 volatile uint32_t* pCFGR;				//OFFSET -> 0x010
 volatile uint32_t* pD1CFGR;			//OFFSET -> 0x018
 volatile uint32_t* pD2CFGR;			//OFFSET -> 0x01C
 volatile uint32_t* pD3CFGR;			//OFFSET -> 0x020

 volatile uint32_t* pPLLCKSELR;		//OFFSET -> 0x028
 volatile uint32_t* pPLLCFGR;			//OFFSET -> 0x02C

 volatile uint32_t* pPLL1DIVR;			//OFFSET -> 0x030
 volatile uint32_t* pPLL1FRACR;		//OFFSET -> 0x034

 volatile uint32_t* pPLL2DIVR;			//OFFSET -> 0x038
 volatile uint32_t* pPLL2FRACR;		//OFFSET -> 0x03C

 volatile uint32_t* pPLL3DIVR;			//OFFSET -> 0x040
 volatile uint32_t* pPLL3FRACR;		//OFFSET -> 0x044

 volatile uint32_t* pD1CCIPR;			//OFFSET -> 0x04C
 volatile uint32_t* pD2CCIP1R;			//OFFSET -> 0x050
 volatile uint32_t* pD2CCIP2R;			//OFFSET -> 0x054
 volatile uint32_t* pD3CCIPR;			//OFFSET -> 0x058
 volatile uint32_t* pCIER;				//OFFSET -> 0x060
 volatile uint32_t* pCIFR;				//OFFSET -> 0x064
 volatile uint32_t* pCICR;				//OFFSET -> 0x068

 volatile uint32_t* pBDCR;				//OFFSET -> 0x070
 volatile uint32_t* pCSR;				//OFFSET -> 0x074

 volatile uint32_t* pAHB3RSTR;			//OFFSET -> 0x07C
 volatile uint32_t* pAHB1RSTR;			//OFFSET -> 0x080
 volatile uint32_t* pAHB2RSTR;			//OFFSET -> 0x084
 volatile uint32_t* pAHB4RSTR;			//OFFSET -> 0x088
 volatile uint32_t* pAPB3RSTR;			//OFFSET -> 0x08C
 volatile uint32_t* pAPB1LRSTR;		//OFFSET -> 0x090

 volatile uint32_t* pAPB1HRSTR;		//OFFSET -> 0x094
 volatile uint32_t* pAPB2RSTR;			//OFFSET -> 0x098
 volatile uint32_t* pAPB4RSTR;			//OFFSET -> 0x09C

 volatile uint32_t* pGCR;				//OFFSET -> 0x0A0
 volatile uint32_t* pD3AMR;			//OFFSET -> 0x0A8
 volatile uint32_t* pRSR;				//OFFSET -> 0x0D0

 volatile uint32_t* pAHB3ENR;			//OFFSET -> 0x0D4
 volatile uint32_t* pAHB1ENR;			//OFFSET -> 0x0D8
 volatile uint32_t* pAHB2ENR;			//OFFSET -> 0x0DC

 volatile uint32_t* pAHB4ENR;			//OFFSET -> 0x0E0
 volatile uint32_t* pAPB3ENR;			//OFFSET -> 0x0E4
 volatile uint32_t* pAPB1LENR;			//OFFSET -> 0x0E8
 volatile uint32_t* pAPB1HENR;			//OFFSET -> 0x0EC
 volatile uint32_t* pAPB2ENR;			//OFFSET -> 0x0F0

 volatile uint32_t* pAPB4ENR;			//OFFSET -> 0x0F4
 volatile uint32_t* pAHB3LPENR;		//OFFSET -> 0x0FC
 volatile uint32_t* pAHB1LPENR;		//OFFSET -> 0x100
 volatile uint32_t* pAHB2LPENR;		//OFFSET -> 0x104
 volatile uint32_t* pAHB4LPENR;		//OFFSET -> 0x108
 volatile uint32_t* pAPB3LPENR;		//OFFSET -> 0x10C
 volatile uint32_t* pAPB1LLPENR;		//OFFSET -> 0x110
 volatile uint32_t* pAPB1HLPENR;		//OFFSET -> 0x114

 volatile uint32_t* pAPB2LPENR;		//OFFSET -> 0x118
 volatile uint32_t* pAPB4LPENR;		//OFFSET -> 0x11C

 volatile uint32_t* pC1_AHB3ENR;		//OFFSET -> 0x134
 volatile uint32_t* pC1_AHB1ENR;		//OFFSET -> 0x138
 volatile uint32_t* pC1_AHB2ENR;		//OFFSET -> 0x13C
 volatile uint32_t* pC1_AHB4ENR;		//OFFSET -> 0x140

 volatile uint32_t* pC1_APB3ENR;			//OFFSET -> 0x144
 volatile uint32_t* pC1_APB1LENR;			//OFFSET -> 0x148
 volatile uint32_t* pC1_APB1HENR;			//OFFSET -> 0x14C
 volatile uint32_t* pC1_APB2ENR;			//OFFSET -> 0x150
 volatile uint32_t* pC1_APB4ENR;			//OFFSET -> 0x154

 volatile uint32_t* pC1_AHB3LPENR;		//OFFSET -> 0x15C
 volatile uint32_t* pC1_AHB1LPENR;		//OFFSET -> 0x160
 volatile uint32_t* pC1_AHB2LPENR;		//OFFSET -> 0x164
 volatile uint32_t* pC1_AHB4LPENR;		//OFFSET -> 0x168
 volatile uint32_t* pC1_APB3LPENR;		//OFFSET -> 0x16C
 volatile uint32_t* pC1_APB1LLPENR;		//OFFSET -> 0x170
 volatile uint32_t* pC1_APB1HLPENR;		//OFFSET -> 0x174
 volatile uint32_t* pC1_APB2LPENR;		//OFFSET -> 0x178
 volatile uint32_t* pC1_APB4LPENR;		//OFFSET -> 0x17C
 public:
 RCC(uint32_t rcc_baseAddress){
 this->CR 			= ((uint32_t*)rcc_baseAddress + 0x0U);				//OFFSET -> 0x000

 this->ICSR= ((uint32_t*)rcc_baseAddress + 0x004U);				//OFFSET -> 0x004U //TODO: CHECAR ESSES...
 this->HSICFGR= ((uint32_t*)rcc_baseAddress + 0x004U);			//OFFSET -> 0x004U


 this->CRRCR= ((uint32_t*)rcc_baseAddress + 0x008U);			//OFFSET -> 0x008U

 this->CFGR= ((uint32_t*)rcc_baseAddress + 0x010U);				//OFFSET -> 0x010U
 this->D1CFGR= ((uint32_t*)rcc_baseAddress + 0x018U);			//OFFSET -> 0x018U
 this->D2CFGR= ((uint32_t*)rcc_baseAddress + 0x01CU);			//OFFSET -> 0x01CU
 this->D3CFGR= ((uint32_t*)rcc_baseAddress + 0x020U);			//OFFSET -> 0x020U

 this->PLLCKSELR= ((uint32_t*)rcc_baseAddress + 0x028U);		//OFFSET -> 0x028U
 this->PLLCFGR= ((uint32_t*)rcc_baseAddress + 0x02CU);			//OFFSET -> 0x02CU

 this->PLL1DIVR= ((uint32_t*)rcc_baseAddress + 0x030U);			//OFFSET -> 0x030U
 this->PLL1FRACR= ((uint32_t*)rcc_baseAddress + 0x034U);		//OFFSET -> 0x034U

 this->PLL2DIVR= ((uint32_t*)rcc_baseAddress + 0x038U);			//OFFSET -> 0x038U
 this->PLL2FRACR= ((uint32_t*)rcc_baseAddress + 0x03CU);		//OFFSET -> 0x03CU

 this->PLL3DIVR= ((uint32_t*)rcc_baseAddress + 0x040U);			//OFFSET -> 0x040U
 this->PLL3FRACR= ((uint32_t*)rcc_baseAddress + 0x044U);		//OFFSET -> 0x044U

 this->D1CCIPR= ((uint32_t*)rcc_baseAddress + 0x04CU);			//OFFSET -> 0x04CU
 this->D2CCIP1R= ((uint32_t*)rcc_baseAddress + 0x050U);			//OFFSET -> 0x050U
 this->D2CCIP2R= ((uint32_t*)rcc_baseAddress + 0x054U);			//OFFSET -> 0x054U
 this->D3CCIPR= ((uint32_t*)rcc_baseAddress + 0x058U);			//OFFSET -> 0x058U
 this->CIER= ((uint32_t*)rcc_baseAddress + 0x060U);				//OFFSET -> 0x060U
 this->CIFR= ((uint32_t*)rcc_baseAddress + 0x064U);				//OFFSET -> 0x064U
 this->CICR= ((uint32_t*)rcc_baseAddress + 0x068U);				//OFFSET -> 0x068U

 this->BDCR= ((uint32_t*)rcc_baseAddress + 0x070U);				//OFFSET -> 0x070U
 this->CSR= ((uint32_t*)rcc_baseAddress + 0x074U);				//OFFSET -> 0x074U

 this->AHB3RSTR= ((uint32_t*)rcc_baseAddress + 0x07CU);			//OFFSET -> 0x07CU
 this->AHB1RSTR= ((uint32_t*)rcc_baseAddress + 0x080U);			//OFFSET -> 0x080U
 this->AHB2RSTR= ((uint32_t*)rcc_baseAddress + 0x084U);			//OFFSET -> 0x084U
 this->AHB4RSTR= ((uint32_t*)rcc_baseAddress + 0x088U);			//OFFSET -> 0x088U
 this->APB3RSTR= ((uint32_t*)rcc_baseAddress + 0x08CU);			//OFFSET -> 0x08CU
 this->APB1LRSTR= ((uint32_t*)rcc_baseAddress + 0x090U);		//OFFSET -> 0x090U

 this->APB1HRSTR= ((uint32_t*)rcc_baseAddress + 0x094U);		//OFFSET -> 0x094U
 this->APB2RSTR= ((uint32_t*)rcc_baseAddress + 0x098U);			//OFFSET -> 0x098U
 this->APB4RSTR= ((uint32_t*)rcc_baseAddress + 0x09CU);			//OFFSET -> 0x09CU

 this->GCR= ((uint32_t*)rcc_baseAddress + 0x0A0U);				//OFFSET -> 0x0A0U
 this->D3AMR= ((uint32_t*)rcc_baseAddress + 0x0A8U);			//OFFSET -> 0x0A8U
 this->RSR= ((uint32_t*)rcc_baseAddress + 0x0D0U);				//OFFSET -> 0x0D0U

 this->AHB3ENR= ((uint32_t*)rcc_baseAddress + 0x0D4U);			//OFFSET -> 0x0D4U
 this->AHB1ENR= ((uint32_t*)rcc_baseAddress + 0x0D8U);			//OFFSET -> 0x0D8U
 this->AHB2ENR= ((uint32_t*)rcc_baseAddress + 0x0DCU);			//OFFSET -> 0x0DCU

 this->AHB4ENR= ((uint32_t*)rcc_baseAddress + 0x0E0U);			//OFFSET -> 0x0E0U
 this->APB3ENR= ((uint32_t*)rcc_baseAddress + 0x0E4U);			//OFFSET -> 0x0E4U
 this->APB1LENR= ((uint32_t*)rcc_baseAddress + 0x0E8U);			//OFFSET -> 0x0E8U
 this->APB1HENR= ((uint32_t*)rcc_baseAddress + 0x0ECU);			//OFFSET -> 0x0ECU
 this->APB2ENR= ((uint32_t*)rcc_baseAddress + 0x0F0U);			//OFFSET -> 0x0F0U

 this->APB4ENR= ((uint32_t*)rcc_baseAddress + 0x0F4U);			//OFFSET -> 0x0F4U

 this->AHB3LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x0FCU
 this->AHB1LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x100U
 this->AHB2LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x104U
 this->AHB4LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x108U
 this->APB3LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x10CU
 this->APB1LLPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x110U
 this->APB1HLPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x114U

 this->APB2LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x118U
 this->APB4LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x11CU

 this->C1_AHB3ENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x134U
 this->C1_AHB1ENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x138U
 this->C1_AHB2ENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x13CU
 this->C1_AHB4ENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x140U

 this->C1_APB3ENR= ((uint32_t*)rcc_baseAddress + 0x0U);			//OFFSET -> 0x144U
 this->C1_APB1LENR= ((uint32_t*)rcc_baseAddress + 0x0U);			//OFFSET -> 0x148U
 this->C1_APB1HENR= ((uint32_t*)rcc_baseAddress + 0x0U);			//OFFSET -> 0x14CU
 this->C1_APB2ENR= ((uint32_t*)rcc_baseAddress + 0x0U);			//OFFSET -> 0x150U
 this->C1_APB4ENR= ((uint32_t*)rcc_baseAddress + 0x0U);			//OFFSET -> 0x154U

 this->C1_AHB3LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x15CU
 this->C1_AHB1LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x160U
 this->C1_AHB2LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x164U
 this->C1_AHB4LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x168U
 this->C1_APB3LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x16CU
 this->C1_APB1LLPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x170U
 this->C1_APB1HLPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x174U
 this->C1_APB2LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x178U
 this->C1_APB4LPENR= ((uint32_t*)rcc_baseAddress + 0x0U);		//OFFSET -> 0x17CU
 }
 };
 */

typedef struct {
	//uint32_t RCC_BASEADDRESS;

	volatile uint32_t* pCR;					//OFFSET -> 0x000U

	volatile uint32_t* pICSR;				//OFFSET -> 0x004U
	volatile uint32_t* pHSICFGR;				//OFFSET -> 0x004U
	volatile uint32_t* pCRRCR;				//OFFSET -> 0x008U

	volatile uint32_t* pCFGR;				//OFFSET -> 0x010U
	volatile uint32_t* pD1CFGR;				//OFFSET -> 0x018U
	volatile uint32_t* pD2CFGR;				//OFFSET -> 0x01CU
	volatile uint32_t* pD3CFGR;				//OFFSET -> 0x020U

	volatile uint32_t* pPLLCKSELR;			//OFFSET -> 0x028U
	volatile uint32_t* pPLLCFGR;				//OFFSET -> 0x02CU

	volatile uint32_t* pPLL1DIVR;			//OFFSET -> 0x030U
	volatile uint32_t* pPLL1FRACR;			//OFFSET -> 0x034U

	volatile uint32_t* pPLL2DIVR;			//OFFSET -> 0x038U
	volatile uint32_t* pPLL2FRACR;			//OFFSET -> 0x03CU

	volatile uint32_t* pPLL3DIVR;			//OFFSET -> 0x040U
	volatile uint32_t* pPLL3FRACR;			//OFFSET -> 0x044U

	volatile uint32_t* pD1CCIPR;				//OFFSET -> 0x04CU
	volatile uint32_t* pD2CCIP1R;			//OFFSET -> 0x050U
	volatile uint32_t* pD2CCIP2R;			//OFFSET -> 0x054U
	volatile uint32_t* pD3CCIPR;				//OFFSET -> 0x058U
	volatile uint32_t* pCIER;				//OFFSET -> 0x060U
	volatile uint32_t* pCIFR;				//OFFSET -> 0x064U
	volatile uint32_t* pCICR;				//OFFSET -> 0x068U

	volatile uint32_t* pBDCR;				//OFFSET -> 0x070U
	volatile uint32_t* pCSR;					//OFFSET -> 0x074U

	volatile uint32_t* pAHB3RSTR;			//OFFSET -> 0x07CU
	volatile uint32_t* pAHB1RSTR;			//OFFSET -> 0x080U
	volatile uint32_t* pAHB2RSTR;			//OFFSET -> 0x084U
	volatile uint32_t* pAHB4RSTR;			//OFFSET -> 0x088U

	volatile uint32_t* pAPB3RSTR;			//OFFSET -> 0x08CU
	volatile uint32_t* pAPB1LRSTR;			//OFFSET -> 0x090U
	volatile uint32_t* pAPB1HRSTR;			//OFFSET -> 0x094U
	volatile uint32_t* pAPB2RSTR;			//OFFSET -> 0x098U
	volatile uint32_t* pAPB4RSTR;			//OFFSET -> 0x09CU

	volatile uint32_t* pGCR;					//OFFSET -> 0x0A0U
	volatile uint32_t* pD3AMR;				//OFFSET -> 0x0A8U
	volatile uint32_t* pRSR;					//OFFSET -> 0x0D0U

	volatile uint32_t* pAHB3ENR;				//OFFSET -> 0x0D4U
	volatile uint32_t* pAHB1ENR;				//OFFSET -> 0x0D8U
	volatile uint32_t* pAHB2ENR;				//OFFSET -> 0x0DCU
	volatile uint32_t* pAHB4ENR;				//OFFSET -> 0x0E0U

	volatile uint32_t* pAPB3ENR;				//OFFSET -> 0x0E4U
	volatile uint32_t* pAPB1LENR;			//OFFSET -> 0x0E8U
	volatile uint32_t* pAPB1HENR;			//OFFSET -> 0x0ECU
	volatile uint32_t* pAPB2ENR;				//OFFSET -> 0x0F0U
	volatile uint32_t* pAPB4ENR;				//OFFSET -> 0x0F4U

	volatile uint32_t* pAHB3LPENR;			//OFFSET -> 0x0FCU
	volatile uint32_t* pAHB1LPENR;			//OFFSET -> 0x100U
	volatile uint32_t* pAHB2LPENR;			//OFFSET -> 0x104U
	volatile uint32_t* pAHB4LPENR;			//OFFSET -> 0x108U

	volatile uint32_t* pAPB3LPENR;			//OFFSET -> 0x10CU
	volatile uint32_t* pAPB1LLPENR;			//OFFSET -> 0x110U
	volatile uint32_t* pAPB1HLPENR;			//OFFSET -> 0x114U

	volatile uint32_t* pAPB2LPENR;			//OFFSET -> 0x118U
	volatile uint32_t* pAPB4LPENR;			//OFFSET -> 0x11CU

	volatile uint32_t* pC1_AHB3ENR;			//OFFSET -> 0x134U
	volatile uint32_t* pC1_AHB1ENR;			//OFFSET -> 0x138U
	volatile uint32_t* pC1_AHB2ENR;			//OFFSET -> 0x13CU
	volatile uint32_t* pC1_AHB4ENR;			//OFFSET -> 0x140U

	volatile uint32_t* pC1_APB3ENR;			//OFFSET -> 0x144U
	volatile uint32_t* pC1_APB1LENR;			//OFFSET -> 0x148U
	volatile uint32_t* pC1_APB1HENR;			//OFFSET -> 0x14CU
	volatile uint32_t* pC1_APB2ENR;			//OFFSET -> 0x150U
	volatile uint32_t* pC1_APB4ENR;			//OFFSET -> 0x154U

	volatile uint32_t* pC1_AHB3LPENR;		//OFFSET -> 0x15CU
	volatile uint32_t* pC1_AHB1LPENR;		//OFFSET -> 0x160U
	volatile uint32_t* pC1_AHB2LPENR;		//OFFSET -> 0x164U
	volatile uint32_t* pC1_AHB4LPENR;		//OFFSET -> 0x168U
	volatile uint32_t* pC1_APB3LPENR;		//OFFSET -> 0x16CU
	volatile uint32_t* pC1_APB1LLPENR;		//OFFSET -> 0x170U
	volatile uint32_t* pC1_APB1HLPENR;		//OFFSET -> 0x174U
	volatile uint32_t* pC1_APB2LPENR;		//OFFSET -> 0x178U
	volatile uint32_t* pC1_APB4LPENR;		//OFFSET -> 0x17CU
} RCC_t;

void RCCsetup(RCC_t *rcc, uint32_t rcc_baseaddress);

void RCC_AHB4_ENR_PortSetup(RCC_t *rcc, uint32_t portValue);
void RCC_AHB4_ENR_BitSetup(RCC_t *rcc, uint8_t bitToSet);

void RCC_AHB4_RSTR_setup(RCC_t *rcc, uint32_t portValue);

/*!< GPIO STRUCTURE > */
#define GPIOK_BASEADDRES 				0x58022800U
#define GPIOJ_BASEADDRES				0x58022400U
#define GPIOI_BASEADDRES 				0x58022000U
#define GPIOH_BASEADDRES 				0x58021C00U
#define GPIOG_BASEADDRES 				0x58021800U
#define GPIOF_BASEADDRES 				0x58021400U
#define GPIOE_BASEADDRES 				0x58021000U
#define GPIOD_BASEADDRES 				0x58020C00U
#define GPIOC_BASEADDRES 				0x58020800U
#define GPIOB_BASEADDRES				0x58020400U
#define GPIOA_BASEADDRES 				0x58020000U

#define GPIOA							0
#define GPIOB							1
#define GPIOC							2
#define GPIOD							3
#define GPIOE							4
#define GPIOF							5
#define GPIOG							6
#define GPIOH							7
#define GPIOI							8
#define GPIOJ							9
#define GPIOK							10

/*!< MODER OPTIONS >*/
//Reset value:
// 0xABFF FFFF for port A
// 0xFFFF FEBF for port B
// 0xFFFF FFFF for other ports
#define MODE_RESET_PORTA 	0xABFFFFFF // 0xFFFF FEBF for port A
#define MODE_RESET_PORTB 	0xFFFFFEBF // 0xFFFF FEBF for port B
#define MODE_RESET_OTHERS 	0xFFFFFFFF // 0xFFFF FEBF other

#define MODE_INPUT			0	//0b00: Input mode    0b0000 0000
#define	MODE_OUTPUT			1	//0b01: General purpose output mode
#define	MODE_ALT_FUNC		2	//0b10: Alternate function mode
#define	MODE_ANALOG			3	//0b11: Analog mode (reset state)

/*!< TYPER OPTIONS >*/
#define TYPER_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000
#define TYPE_PUSH_PULL		0	//0: Output push-pull (reset state)
#define	TYPE_OPEN_DRAIN		1	//1: Output open-drain

/*!< SPEEDR OPTIONS >*/
#define SPEED_RESET_PORTA 	0x0C000000 //Reset value: 0x0C00 0000 (for port A)
#define SPEED_RESET_PORTB 	0x000000C0 //Reset value: 0x0000 00C0 (for port B)
#define SPEED_RESET_OTHERS 	0x00000000 //Reset value: 0x0000 0000 (for other ports)

#define SPEED_LOW		0	//00: Low speed
#define	SPEED_MEDIUM	1	//01: Medium speed
#define	SPEED_HIGH		2	//10: High speed
#define	SPEED_VHIGH		3	//11: Very high speed

/*!< PUPD OPTIONS >*/
#define PUPD_RESET_PORTA 	 0x64000000 //Reset value: 0x6400 0000 (for port A)
#define PUPD_RESET_PORTB 	 0x00000100 //Reset value: 0x0000 0100 (for port B)
#define PUPD_RESET_OTHERS 	 0x00000000 //Reset value: 0x0000 0000 (for other ports)

#define PUPD_DISABLED		0	//00: No pull-up, pull-down
#define	PUPD_UP				1	//01: Pull-up
#define	PUPD_DOWN			2	//10: Pull-down
#define	PUPD_RESERVED		3	//11: Reserved

/*!< IDR OPTIONS >*/
#define IDR_RESET_PORT 	0x00000000 //Reset value: 0x0000 XXXX

/*!< ODR OPTIONS >*/
#define ODR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

/*!< BSRR OPTIONS >*/
#define BSRR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000
//Bits [31-16] OPTIONS
//0: No action on the corresponding ODRx bit
//1: Resets the corresponding ODRx bit
//Bits [15-0] OPTIONS
//0: No action on the corresponding ODRx bit
//1: Sets the corresponding ODRx bit

/*!< LCKR OPTIONS >*/
#define LCKR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

/*!< AFRL OPTIONS >*/
#define AFRL_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

#define AF0			0 	//0000:	AF0
#define AF1			1 	//0001:	AF1
#define AF2			2 	//0010:	AF2
#define AF3			3 	//0011:	AF3
#define AF4			4 	//0100:	AF4
#define AF5			5 	//0101:	AF5
#define AF6			6 	//0110: AF6
#define AF7			7 	//0111: AF7
#define AF8			8 	//1000: AF8
#define AF9			9 	//1001: AF9
#define AF10		10 	//1010: AF10
#define AF11		11 	//1011: AF11
#define AF12		12 	//1100: AF12
#define AF13		13 	//1101: AF13
#define AF14		14 	//1110: AF14
#define AF15		15 	//1111: AF15

/*!< AFRH OPTIONS >*/
#define AFRH_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

typedef struct {
	volatile uint32_t* pMODER;				//OFFSET -> 0x00
	volatile uint32_t* pOTYPER;				//OFFSET -> 0x04
	volatile uint32_t* pOSPEEDR;			//OFFSET -> 0x08
	volatile uint32_t* pPUPDR;				//OFFSET -> 0x0C
	volatile uint32_t* pIDR;				//OFFSET -> 0x10
	volatile uint32_t* pODR;				//OFFSET -> 0x14
	volatile uint32_t* pBSRR;				//OFFSET -> 0x18
	volatile uint32_t* pLCKR;				//OFFSET -> 0x1C
	volatile uint32_t* pAFRL;				//OFFSET -> 0x20
	volatile uint32_t* pAFRH;				//OFFSET -> 0x24
} GPIOx_t;

void GPIOx_setup(GPIOx_t *pGPIOx, uint32_t GPIO_BASEADDRESS);
void GPIOx_setPortMODE(GPIOx_t *pGPIOx, uint32_t portValue);
void GPIOx_setPortTYPE(GPIOx_t *pGPIOx, uint32_t portValue);
void GPIOx_setPortSPEED(GPIOx_t *pGPIOx, uint32_t portValue);
void GPIOx_setPortPUPD(GPIOx_t *pGPIOx, uint32_t portValue);

uint32_t GPIOx_readPortState(GPIOx_t *pGPIOx);

void GPIOx_setPortState(GPIOx_t *pGPIOx, uint32_t portValue);

void GPIOx_setPortBSR(GPIOx_t *pGPIOx, uint32_t portValue);
void GPIOx_setPortAFRL(GPIOx_t *pGPIOx, uint32_t portValue);
void GPIOx_setPortAFRH(GPIOx_t *pGPIOx, uint32_t portValue);

void PINx_setPinMODE(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t mode);
void PINx_setPinTYPE(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t type);
void PINx_setPinSPEED(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t speed);
void PINx_setPinPUPD(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t resMode);

uint8_t PINx_readPinState(GPIOx_t *pGPIOx, uint8_t pinX);
void PINx_setPinState(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state);

void PINx_setPinBSR(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state);
void PINx_setPinAFRL(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state);
void PINx_setPinAFRH(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state);

/*!< SUPPORT METHODS >*/
uint32_t getTwoBitsRegister(uint32_t registerToSet, uint8_t bitToSet,
		uint8_t valueToSet);
void setTwoBitsRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet);
void setOneBitRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet);

#endif /* STM32H743CUSTOM_H_ */
