/*
 * stm32H7431custom.c
 *
 *  Created on: 6 Feb 2020
 *      Author: Erick Cabral
 */

#include"stm32H743custom.h"

void RCCsetup(RCC_t *rcc, uint32_t rcc_baseaddress){
	//rcc->RCC_BASEADDRESS = rcc_baseaddress;
	rcc->pAHB4ENR = (uint32_t*)(rcc_baseaddress +  0x0E0U);
	*(rcc->pAHB4ENR) = 0x00000000; //Reset Value
}

void RCC_AHB4_ENR_PortSetup(RCC_t *rcc, uint32_t portValue){
//	rcc->pAHB4ENR = (uint32_t*)(rcc->RCC_BASEADDRESS +  0x0E0U);

	*(rcc->pAHB4ENR) |= portValue;
}
void RCC_AHB4_ENR_BitSetup(RCC_t *rcc, uint8_t bitToSet){
//	rcc->pAHB4ENR = (uint32_t*)(rcc->RCC_BASEADDRESS +  0x0E0U);
	// Bit 11 - 18| 20| 22-23 | 26-27 | 29 - 31 are RESERVED...,
	if((bitToSet >= 11 && bitToSet <=18 ) ||
			(bitToSet == 20) ||
			(bitToSet >= 22 && bitToSet <=23)
			|| (bitToSet >= 26 && bitToSet <=27)
			|| (bitToSet >= 29 && bitToSet <=31) ){
		//RESERVED BITS
		return;
	}else{
		*(rcc->pAHB4ENR) |= (1 << bitToSet);
	}

}

void RCC_AHB4_RSTR_PortSetup(RCC_t *rcc, uint32_t portValue){
//	rcc->pAHB4RSTR = (uint32_t*)(rcc->RCC_BASEADDRESS +  0x088U); //OFFSET -> 0x088U
	*(rcc->pAHB4RSTR) = portValue;
}



void GPIOx_setup(GPIOx_t *pGPIOx, uint32_t gpio_baseAddress){
	pGPIOx->pMODER = (uint32_t*) (gpio_baseAddress);	//OFFSET -> 0x00U
	if (gpio_baseAddress == GPIOA_BASEADDRES) {
		*(pGPIOx->pMODER) = MODE_RESET_PORTA;
	} else if (gpio_baseAddress == GPIOB_BASEADDRES) {
		*(pGPIOx->pMODER) = MODE_RESET_PORTB;
	} else {
		*(pGPIOx->pMODER) = MODE_RESET_OTHERS;
	}

	/*!< TYPER OPTIONS >*/
	pGPIOx->pOTYPER = (uint32_t*) (gpio_baseAddress + 0x04U);//OFFSET -> 0x04U
	*(pGPIOx->pOSPEEDR) =  TYPER_RESET_PORT; 	//Reset value: 0x0000 0000

	/*!< SPEED OPTIONS >*/
	pGPIOx->pOSPEEDR = (uint32_t*) (gpio_baseAddress + 0x08U);//OFFSET -> 0x08U
	if (gpio_baseAddress == GPIOA_BASEADDRES) {
		*(pGPIOx->pOSPEEDR) = SPEED_RESET_PORTA;
	} else if (gpio_baseAddress == GPIOB_BASEADDRES) {
		*(pGPIOx->pOSPEEDR) = SPEED_RESET_PORTB;
	} else {
		*(pGPIOx->pOSPEEDR) = SPEED_RESET_OTHERS;
	}

	pGPIOx->pPUPDR = (uint32_t*) (gpio_baseAddress + 0x0CU);//OFFSET -> 0x0CU
	if (gpio_baseAddress == GPIOA_BASEADDRES) {
		*(pGPIOx->pPUPDR) = PUPD_RESET_PORTA;
	} else if (gpio_baseAddress == GPIOB_BASEADDRES) {
		*(pGPIOx->pPUPDR) = PUPD_RESET_PORTB;
	} else {
		*(pGPIOx->pPUPDR) = PUPD_RESET_OTHERS;
	}
	/*!< IDR OPTIONS >*/
	pGPIOx->pIDR = (uint32_t*) (gpio_baseAddress + 0x10U);	//OFFSET -> 0x10U
	*(pGPIOx->pIDR) = IDR_RESET_PORT; //Reset value: 0x0000 XXXX

	/*!< ODR OPTIONS >*/
	pGPIOx->pODR = (uint32_t*) (gpio_baseAddress + 0x14U);	//OFFSET -> 0x14U
	*(pGPIOx->pODR) = ODR_RESET_PORT; //Reset value: 0x0000 0000

	/*!< BSRR OPTIONS >*/
	pGPIOx->pBSRR = (uint32_t*) (gpio_baseAddress + 0x18U);	//OFFSET -> 0x18U
	*(pGPIOx->pBSRR) = BSRR_RESET_PORT; //Reset value: 0x0000 0000

	/*!< LCKR OPTIONS >*/
	pGPIOx->pLCKR = (uint32_t*) (gpio_baseAddress + 0x1CU);	//OFFSET -> 0x1CU
	*(pGPIOx->pAFRL) = LCKR_RESET_PORT; //Reset value: 0x0000 0000

	/*!< AFRL OPTIONS >*/
	pGPIOx->pAFRL = (uint32_t*) (gpio_baseAddress + 0x20U);	//OFFSET -> 0x20U
	*(pGPIOx->pAFRL) = LCKR_RESET_PORT; //Reset value: 0x0000 0000

	/*!< AFRH OPTIONS >*/
	pGPIOx->pAFRH = (uint32_t*) (gpio_baseAddress + 0x24U);	//OFFSET -> 0x24U
	*(pGPIOx->pAFRH) = AFRH_RESET_PORT; //Reset value: 0x0000 0000
}

void GPIOx_setPortMODE(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pMODER) = portValue;
}
void GPIOx_setPortTYPE(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pOTYPER) = portValue;
}
void GPIOx_setPortSPEED(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pOSPEEDR) = portValue;
}
void GPIOx_setPortPUPD(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pPUPDR) = portValue;
}
void GPIOx_setPortIDR(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pIDR) = portValue;
}
void GPIOx_setPortState(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pODR) = portValue;
}

uint32_t GPIOx_readPortState(GPIOx_t *pGPIOx){
	return *(pGPIOx->pODR);
}
void GPIOx_setPortBSR(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pBSRR) = portValue;
}
void GPIOx_setPortAFRL(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pAFRL) = portValue;
}
void GPIOx_setPortAFRH(GPIOx_t *pGPIOx, uint32_t portValue){
	*(pGPIOx->pAFRH) = portValue;
}


void PINx_setPinMODE(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t mode){
	setTwoBitsRegister(pGPIOx->pMODER, pinX, mode);
}

void PINx_setPinTYPE(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t type){
	setOneBitRegister(pGPIOx->pOTYPER, pinX, type);
}
void PINx_setPinSPEED(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t speed){
	setTwoBitsRegister(pGPIOx->pOSPEEDR, pinX, speed);
}
void PINx_setPinPUPD(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t resMode){
	setTwoBitsRegister(pGPIOx->pPUPDR, pinX, resMode);
}

uint8_t PINx_readPinState(GPIOx_t *pGPIOx, uint8_t pinX){
	uint8_t pinState = (*(pGPIOx->pIDR)>>(pinX));
	return pinState;
}

void PINx_setPinState(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state){
	if(state == HIGH){
		*(pGPIOx->pODR) |= (1 << pinX);
	}else{
		*(pGPIOx->pODR) &= ~(1 << pinX);
	}
}

void PINx_setPinBSR(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state){
	setOneBitRegister(pGPIOx->pOTYPER, pinX, state);
}
void PINx_setPinAFRL(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state){
	//4 Bit Register to set
}
void PINx_setPinAFRH(GPIOx_t *pGPIOx, uint8_t pinX, uint8_t state){
	//4 Bit Register to set
}


/*!< SUPPORT METHODS >*/

uint32_t getTwoBitsRegister(uint32_t registerToSet,uint8_t bitToSet, uint8_t valueToSet){
	uint32_t bit1 = 0;
	switch(valueToSet){
	case MODE_INPUT:
		registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2* bitToSet));
		bit1 |= (0<<(2*bitToSet+1)| 0<<(2*bitToSet));
		return bit1;
	case MODE_OUTPUT:
		registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (0<<(2*bitToSet+1)| 1<<(2*bitToSet));
		return bit1;
	case MODE_ALT_FUNC:
		registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (1<<(2*bitToSet+1)| 0<<(2*bitToSet));
		break;
	case MODE_ANALOG:
		registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (1<<(2*bitToSet+1)| 1<<(2*bitToSet));
		return bit1;
	}
	return 0;
}

void setTwoBitsRegister(volatile uint32_t *registerToSet,uint8_t bitToSet, uint8_t valueToSet){
	uint32_t bit1 = 0;
	switch(valueToSet){
	case 0:
		*registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2* bitToSet));
		bit1 |= (0<<(2*bitToSet+1)| 0<<(2*bitToSet));
		break;
	case 1:
		*registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (0<<(2*bitToSet+1)| 1<<(2*bitToSet));
		break;
	case 2:
		*registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (1<<(2*bitToSet+1)| 0<<(2*bitToSet));
		break;
	case 3:
		*registerToSet &= ~(1<< (2*bitToSet +1)| 1<< (2*bitToSet));
		bit1 |= (1<<(2*bitToSet+1)| 1<<(2*bitToSet));
		break;
	}
	*registerToSet |= bit1;
}

void setOneBitRegister(volatile uint32_t *registerToSet,uint8_t bitToSet, uint8_t valueToSet){
	uint32_t bit1 = 0;
	*registerToSet &= ~(1<< (bitToSet));
	bit1 |= (valueToSet<<(2*bitToSet));
	*registerToSet |= bit1;
}








